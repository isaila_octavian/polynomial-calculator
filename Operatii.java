

public class Operatii { 
	
	
	public Polinom p1;
	public Polinom p2;
	public PolinomF p3;

	
	Operatii()
	{
		
	}
	
	Operatii(Polinom pp1)
	{
		this.p1=pp1;
	}
	
	Operatii(PolinomF pp1)
	{
		this.p3=pp1;
	}
	
	
   Operatii(Polinom pp1, Polinom pp2)
   {
	   this.p1=pp1;
	   this.p2=pp2;
   }
   
	
	public Polinom adunare(Polinom poli2, Polinom poli1) {
	    
	     Polinom poli3 = poli1;
	     

	     for(int i=0;i<poli3.getSize();i++) {
	       
	          int p1 = poli1.get(i).getGrad();
	          int c1 = poli1.get(i).getCoeficient();          
	          
	          for(int j = 0; j < poli2.getSize(); j++) {
	        	  if(poli2.get(j).getGrad() == p1) {
	        		  poli1.get(i).setCoeficient(c1 + poli2.get(j).getCoeficient());
	        		  poli2.poli.remove(j);
	        	  }
	          }
	     }

	     for(int j=0;j<poli2.poli.size();j++) {
	          int p2 = poli2.poli.get(j).getGrad();
	          int c2 = poli2.poli.get(j).getCoeficient();
	          poli3.poli.add(new Monom(c2, p2));
	     }
	     return poli3;
	}
	
	
	public Polinom scadere(Polinom poli2, Polinom poli1) {
	    
	     Polinom poli3 = poli1;
	     

	     for(int i=0;i<poli3.getSize();i++) {
	         
	          int p1 = poli1.get(i).getGrad();
	          int c1 = poli1.get(i).getCoeficient();          
	          
	          for(int j = 0; j < poli2.getSize(); j++) {
	        	  if(poli2.get(j).getGrad() == p1) {
	        		  poli1.get(i).setCoeficient(c1 - poli2.get(j).getCoeficient());
	        		  poli2.poli.remove(j);
	        	  }
	          }
	     }

	     for(int j=0;j<poli2.poli.size();j++) {
	          int p2 = poli2.poli.get(j).getGrad();
	          int c2 = poli2.poli.get(j).getCoeficient();
	          poli3.poli.add(new Monom(-c2, p2));
	     }
	     return poli3;
	}
	
	
	public Polinom derivare(Polinom poli1)
	{
		
		Polinom poli3 = poli1;
		
		for( int i=0; i<poli3.getSize(); i++)
		{
			int p1 = poli1.get(i).getGrad();
	        int c1 = poli1.get(i).getCoeficient(); 
	        if(p1!=0) {
	        	poli3.get(i).setCoeficient(p1*c1);
	        	poli3.get(i).setGrad(p1-1);
	        	
	        }
	        else {
	        	poli3.get(i).setCoeficient(0);
	        }
		}
		
		return poli3;
	}
	
	
	public PolinomF integrare(PolinomF poli1)
	{
		PolinomF poli3 = poli1;
		
		for( int i=0; i<poli3.getSize(); i++)
		{
			float p1 = poli1.get(i).getGradF();
	        float c1 = poli1.get(i).getCoeficientF(); 
	        poli3.get(i).setGradF(p1+1);
	        poli3.get(i).setCoeficientF(c1/(p1+1));
		}
		
		return poli3;
	}
	
	public Polinom inmultire(Polinom poli1, Polinom poli2) {
		
		Polinom poli3=new Polinom(1);
		
		System.out.println(poli1.getSize());
		System.out.println(poli2.getSize());
	     for(int i=0;i<poli1.getSize();i++) {
	        
	    	 int c1=poli1.get(i).getCoeficient();
	    	 int p1=poli1.get(i).getGrad();
	          for(int j=0;j<poli2.getSize();j++) {
	                
	        	 int c2=poli2.get(j).getCoeficient();
	 	    	 int p2=poli2.get(j).getGrad();
	               
	 	    	int c3=c1*c2;
	 	    	int p3=p2+p1;
	            poli3.poli.add(new Monom(c3, p3));
	          }
	     }
	     return poli3;
	}
}
	
   

