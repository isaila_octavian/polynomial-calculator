
import java.util.ArrayList;

public class PolinomF {
	ArrayList<MonomF> poli = new ArrayList<MonomF>();
	private String polinom;
	

	public int getSize() {
		return poli.size();
	}
	
	
	public ArrayList<MonomF> getPoli() {
		return poli;
	}

	public void setPoli(ArrayList<MonomF> poli) {
		this.poli = poli;
	}

	public String getPolinom() {
		return polinom;
	}

	public void setPolinom(String polinom) {
		this.polinom = polinom;
	}

	PolinomF(String Polinom)
	{
	this.polinom=Polinom;
	float coef=0; int grad=0;
	ArrayList<String> p = new ArrayList<>();
	String[] pol=polinom.split("x| ");
	
	
	for (int a=0; a< pol.length; a++)
    {
    p.add(pol[a]);
    }
	
	System.out.println(p);
	for (int a=0; a< pol.length; a=a+2)
    {
    coef=Integer.parseInt(pol[a]);
    grad=Integer.parseInt(pol[a+1]);
    MonomF m=new MonomF(coef,grad);
    poli.add(m);
    }
	}
	
	public MonomF get(int i)
	{
		return poli.get(i);
	}
	public String toString()
	{
		String polinom="";
		for(int j=0;j<poli.size();j++) {
	          float pu = poli.get(j).getGradF();
	          float co = poli.get(j).getCoeficientF();
	          polinom+=co +"x^" + pu+ " ";
	     }
		return polinom;
	}
	
	
}

