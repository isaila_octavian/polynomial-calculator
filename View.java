import java.lang.String;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

class View extends JFrame implements ActionListener {
    
    private JTextField m_userInputTf = new JTextField(170);
    private JTextField m_userInputTf1 = new JTextField(170);
    private JTextField m_totalTf     = new JTextField(170);
    private JButton    m_multiplyBtn1 = new JButton("Adunare");
    private JButton    m_multiplyBtn2 = new JButton("Scadere");
    private JButton    m_multiplyBtn3 = new JButton("Derivare");
    private JButton    m_multiplyBtn4 = new JButton("Integrare");
    private JButton    m_multiplyBtn5 = new JButton("Inmultire");
    
    Operatii m_model;
    
    View(Operatii model) {
        
        m_model = model;
        
        
        m_totalTf.setEditable(true);
        this.setSize(200, 300);
             
        JPanel content = new JPanel();
   
         
        content.setLayout(new FlowLayout());
        content.add(new JLabel("Primul Polinom"));
        content.add(m_userInputTf);
        
        content.add(new JLabel("Al Doilea Polinom"));
        content.add(m_userInputTf1);
        
        content.add(m_multiplyBtn1);
        m_multiplyBtn1.addActionListener(this);
        content.add(m_multiplyBtn2);
        m_multiplyBtn2.addActionListener(this);
        content.add(m_multiplyBtn3);
        m_multiplyBtn3.addActionListener(this);
        content.add(m_multiplyBtn4);
        m_multiplyBtn4.addActionListener(this);
        content.add(m_multiplyBtn5);
        m_multiplyBtn5.addActionListener(this);
        content.add(new JLabel("Rezultatul calcului este"));
        content.add(m_totalTf);
        
       
        this.setContentPane(content);
        this.pack();
        
        
        this.setTitle("Calculator Polinoame");
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

	
	public void actionPerformed(ActionEvent arg0) {
		
		Object sursa=arg0.getSource();
		
		Polinom poli1= new Polinom(m_userInputTf.getText());
		Polinom poli2= new Polinom(m_userInputTf1.getText());
		PolinomF poli3= new PolinomF(m_userInputTf1.getText());
		
		if(sursa==m_multiplyBtn1) {
			Operatii d1 = new Operatii(poli1,poli2);
	        System.out.println(d1.adunare(poli2, poli1));
		
			m_totalTf.setText((d1.adunare(poli2, poli1)).toString());
		
		}
		
		if(sursa==m_multiplyBtn2) {
			Operatii d1 = new Operatii(poli1,poli2);
	        System.out.println(d1.scadere(poli2, poli1));
		
			m_totalTf.setText((d1.scadere(poli2, poli1)).toString());
		
		}
		
		if(sursa==m_multiplyBtn3) {
			Operatii d1 = new Operatii(poli1);
	        //System.out.println(d1.derivare(poli1));
		
			m_totalTf.setText((d1.derivare(poli1)).toString());
		
		}
		
		if(sursa==m_multiplyBtn4) {
			Operatii d1 = new Operatii(poli3);
	        //System.out.println(d1.integrare(poli3));
		
			m_totalTf.setText(d1.integrare(poli3).toString());
		
		}
		
		if(sursa==m_multiplyBtn5) {
			Operatii d1 = new Operatii(poli1,poli2);
	        System.out.println(d1.inmultire(poli2, poli1));
		
			m_totalTf.setText((d1.inmultire(poli2, poli1)).toString());
		
		}
		
		if(sursa==m_multiplyBtn1 || sursa==m_multiplyBtn2 || sursa==m_multiplyBtn3 || sursa==m_multiplyBtn4 || sursa==m_multiplyBtn5) 
			if((m_userInputTf.getText().length())<=2  || (m_userInputTf1.getText().length())<=2) JOptionPane.showMessageDialog(this, "Ati introdus un input gresit");
		
		
		
	}
	
}

