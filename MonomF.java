public class MonomF{
	private float grad;
	private float coeficient;
	
	public MonomF(float coeficient, float grad)
	{
	this.grad=grad;
	this.coeficient=coeficient;
	}

	public float getGradF() {
		return grad;
	}

	public void setGradF(float grad) {
		this.grad = grad;
	}

	public float getCoeficientF() {
		return coeficient;
	}

	public void setCoeficientF(float coeficient) {
		this.coeficient = coeficient;
	}
	
}


