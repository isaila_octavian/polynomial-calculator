public class Monom{
	private int grad;
	private int coeficient;
	
	public Monom(int coeficient, int grad)
	{
	this.grad=grad;
	this.coeficient=coeficient;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public int getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}
	
}

