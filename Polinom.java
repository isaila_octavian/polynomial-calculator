import java.util.ArrayList;

public class Polinom {
	ArrayList<Monom> poli = new ArrayList<Monom>();
	private String polinom;
	

	public int getSize() {
		return poli.size();
	}
	
	
	public ArrayList<Monom> getPoli() {
		return poli;
	}

	public void setPoli(ArrayList<Monom> poli) {
		this.poli = poli;
	}

	public String getPolinom() {
		return polinom;
	}

	public void setPolinom(String polinom) {
		this.polinom = polinom;
	}
	
	Polinom(int a){
		
	}

	Polinom(String Polinom)
	{
	this.polinom=Polinom;
	int coef=0, grad=0;
	ArrayList<String> p = new ArrayList<>();
	String[] pol=polinom.split("x| ");
	
	
	for (int a=0; a< pol.length; a++)
    {
    p.add(pol[a]);
    }
	
	System.out.println(p);
	for (int a=0; a< pol.length-1; a=a+2)
    {
    coef=Integer.parseInt(pol[a]);
    grad=Integer.parseInt(pol[a+1]);
    Monom m=new Monom(coef,grad);
    poli.add(m);
    }
	}
	
	public Monom get(int i)
	{
		return poli.get(i);
	}
	public String toString()
	{
		String polinom="";
		
		for(int j=0;j<poli.size();j++) {
	          int pu = poli.get(j).getGrad();
	          int co = poli.get(j).getCoeficient();
	          polinom+=co +"x^" + pu+ " ";
	     }
		return polinom;
	}
	
	
}
