

import static org.junit.Assert.*;
import org.junit.*;

public class JUnitTest {

	
	private String po1="3x4 5x2 -3x0";
	private String po2="2x3 1x1 8x0";
	private String po3="1x0";
	private String po4="0x0";
	
	
	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0;
	
	private static Polinom p1;
	private static Polinom p2;
	private static Polinom p3;
	private static Polinom p4;
	private static PolinomF p5;
	
	public JUnitTest()
	{
		System.out.println("Constructor inaintea fiecarui test!");
		p1=new Polinom(po1);
		p2=new Polinom(po2);
		p3=new Polinom(po3);
		p4=new Polinom(po4);
		p5=new PolinomF(po1);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		p1=new Polinom(1);
		p2=new Polinom(1);
		p3=new Polinom(1);
		p4=new Polinom(1);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("O singura data dupa terminarea executiei setului de teste din clasa!");
		System.out.println("S-au executat " + nrTesteExecutate + " teste din care "+ nrTesteCuSucces + " au avut succes!");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Incepe un nou test!");
		nrTesteExecutate++;
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("S-a terminat testul curent!");
	}	
	
	@Test
	public void testAdunare() {
		 String rezultat="";
		 Operatii m = new Operatii(p1,p2);
		 rezultat=(m.adunare(p1, p2)).toString();
		assertNotNull(rezultat); 
		assertEquals(rezultat,"2x^3 1x^1 5x^0 3x^4 5x^2 "); 
		nrTesteCuSucces++;		
	}
	
	@Test
	public void testScadere() {
		String rezultat="";
		 Operatii m = new Operaii(p1,p2);
		 rezultat=(m.scadere(p1, p2)).toString();
		assertNotNull(rezultat); 
		assertEquals(rezultat,"2x^3 1x^1 11x^0 -3x^4 -5x^2 "); 
		nrTesteCuSucces++;		
	}	

	@Test
	public void testDerivare() {
		String rezultat="";
		 Operatii m = new Operatii(p1);
		 rezultat=(m.derivare(p1)).toString();
		assertNotNull(rezultat); 
		assertEquals(rezultat,"12x^3 10x^1 0x^0 "); 
		nrTesteCuSucces++;
	}

	@Test
	public void testIntegrare() {
		String rezultat="";
		 Operatii m = new Operatii(p5);
		 rezultat=(m.integrare(p5)).toString();
		assertNotNull(rezultat); 
		assertEquals(rezultat,"0.6x^5.0 1.6666666x^3.0 -3.0x^1.0 "); 
		nrTesteCuSucces++;
	}
	
	@Test
	public void testInmultire1() {
		String rezultat="";
		 Operatii m = new Operatii(p1,p3);
		 rezultat=(m.inmultire(p1, p3)).toString();
		assertNotNull(rezultat); 
		assertEquals(rezultat,"3x^4 5x^2 -3x^0 "); 
		nrTesteCuSucces++;		
	}
	
	@Test
	public void testInmultire0() {
		String rezultat="";
		 Operatii m = new Operatii(p1,p4);
		 rezultat=(m.inmultire(p1, p4)).toString();
		assertNotNull(rezultat); 
		assertEquals(rezultat,"0x^4 0x^2 0x^0 "); 
		nrTesteCuSucces++;		
	}

}
